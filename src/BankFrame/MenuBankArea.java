package BankFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class MenuBankArea extends JFrame{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 200;
   private static final double DEFAULT_RATE = 0;
   private static final double INITIAL_BALANCE = 0;   
   private JLabel rateLabel;
   private JTextField rateField;
   private JTextArea balanceArea;
   private JButton button1;
   private JButton button2;
   public JLabel resultLabel;
   public JLabel showLable;
   private JPanel panel;
   
   private BankAccount account;
   private JPanel colorPanel;
  	private JMenuItem searchItem, onlineHelpItem, SimpleItem, ComplexItem, InsertItem, DeleteItem;
  	private String facename;

   public MenuBankArea(){
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(createOptionMenu());
		menuBar.add(createHelpMenu());
		menuBar.add(createDataMenu());
		menuBar.add(createTransactionMenu());
		createFrameItem(facename);
		
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		account = new BankAccount(INITIAL_BALANCE);

	      // Use instance variables for components 
	      resultLabel = new JLabel("Your balance: " + 0);

	      // Use helper methods 
	      createTextField();
	      createButtonDeposit();
	      createButtonWithdraw();
	      createPanel();
	      
	      setVisible(true);
	      setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	public JMenu createOptionMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("Option");
		menu.add(createOptionExitItem());
		
		return menu;
	}
	
	public JMenu createHelpMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("Help");
		menu.add(createFrameMenu());
		return menu;
	}
	
	public JMenu createDataMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("Data");
		menu.add(createDataChoiceMenu());
		return menu;
	}
	
	public JMenu createTransactionMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("Transaction");
		menu.add(createTransChoiceMenu());
		return menu;
	} 	
	
	public JMenuItem createFileNewItem(){
		JMenuItem item = new JMenuItem("New");
		 class MenuItemListener implements ActionListener{
			 public void actionPerformed(ActionEvent event){
				 
			 }
		 }
		 	ActionListener listener = new MenuItemListener();
		 	item.addActionListener(listener);
		 	return item;
	}
	
	public JMenuItem createOptionExitItem()
	{
	 JMenuItem item = new JMenuItem("Exit");
	 class MenuItemListener implements ActionListener{
		 public void actionPerformed(ActionEvent event){
			 System.exit(0);
		 }
	 }
	 	ActionListener listener = new MenuItemListener();
	 	item.addActionListener(listener);
	 	return item;
	 }
		
	public JMenuItem createAddTransactionMenu(){
		JMenuItem item = new JMenuItem("Transaction");
		 class MenuItemListener implements ActionListener{
			 public void actionPerformed(ActionEvent event){
				 
			 }
		 }
		 	ActionListener listener = new MenuItemListener();
		 	item.addActionListener(listener);
		 	return item;
	}
	
	public JMenu createFrameMenu()
	 {	 
		 JMenu menu = new JMenu("Select Help");
		 menu.add(searchItem = new JMenuItem("Search"));
		 menu.add(onlineHelpItem = new JMenuItem("Online Help"));
		 
		 return menu;
	 }
	
	public JMenu createTransChoiceMenu()
	 {	 
		 JMenu menu = new JMenu("Select");
		 menu.add(InsertItem = new JMenuItem("Insert"));
		 menu.add(DeleteItem = new JMenuItem("Delete"));
		 
		 return menu;
	 }
	
	public JMenu createDataChoiceMenu()
	 {	 
		 JMenu menu = new JMenu("Select Data");
		 menu.add(SimpleItem = new JMenuItem("Simple"));
		 menu.add(ComplexItem = new JMenuItem("Complex"));
		 
		 return menu;
	 }
	public JMenuItem createFrameItem(final String name)
	 {
		JMenuItem item = new JMenuItem(name);
		 class MenuItemListener implements ActionListener{
		 public void actionPerformed(ActionEvent event)
		 {
			
			 if(event.getSource() == searchItem){
				 
				 facename = name;
				
			 }
			 else if(event.getSource() == onlineHelpItem){
				 
				 facename = name;
			 }
			
		 	}
		 }
		 ActionListener listener = new MenuItemListener();
		 item.addActionListener(listener);
		 return item;
	 }

	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
		}
	}
	
	public static void main(String[] args){
		   new MenuBank();
	   }
	
	   private void createTextField()
	   {
	      rateLabel = new JLabel("Money in your BankAccount:  100");

	      final int FIELD_WIDTH = 10;
	      rateField = new JTextField(FIELD_WIDTH);
	      rateField.setText("" + DEFAULT_RATE);
	   }
	   
	   public JButton getButton1(){
		   button1 = new JButton("Add Deposit");
		   return button1;
	   }
	   
	   public JButton getButton2(){
		   button2 = new JButton("Add Interest");
		   return button2;
	   }
	   public JTextField getField(){
		   return rateField;
	   }
	   
	   public JLabel getLabel(){
		   return resultLabel;
	   }
	   
	   private void createButtonDeposit()
	   {
	      button1 = new JButton("Add Deposit");
	      class AddInterestListener implements ActionListener{
	       public void actionPerformed(ActionEvent event){
		       double Money = Double.parseDouble(rateField.getText());
		       double balance = account.getBalance() + Money;
		       account.deposit(balance);
		       //rateField.append(account.getBalance() + "\n");
	       }
	     
	      }
	   }  
	   
	   private void createButtonWithdraw()
	   {
	      button2 = new JButton("Add Withdraw");
	      class AddInterestListener implements ActionListener{
		       public void actionPerformed(ActionEvent event){
			       double Money = Double.parseDouble(rateField.getText());
			       double balance = account.getBalance() - Money;
			       account.deposit(balance);
			       balanceArea.append(account.getBalance() + "\n");
		       }
		     
		      }
	   }  
	   
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(button1);
      panel.add(button2);
      //panel.add(resultLabel); 
      panel.add(balanceArea);
      add(panel);
   }

}
