package BankFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
	private ActionListener listener ;
	private BankAccount bank ;
	private MenuBank show ;
	
	public Controller()
	{	bank = new BankAccount(0);
		show = new MenuBank();
		listener = new AddInterestListener();
		show.getButton1().addActionListener(listener);
		show.getButton2().addActionListener(listener);
		show.getLabel().setText("balance: " + bank.getBalance());
	}
	
	public static void main(String[] args){
		   new Controller();
	   }
	
	  class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            if(show.getButton1() != null){
        	double amount = Double.parseDouble(show.getField().getText());
            double money = bank.getBalance();
            bank.deposit(money);
            show.getLabel().setText("balance: " + bank.getBalance());
            }
         }            
      }
      
	public Object getField() {
		// TODO Auto-generated method stub
		return null;
	}
     
   }


