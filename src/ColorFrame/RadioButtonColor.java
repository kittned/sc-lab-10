package ColorFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class RadioButtonColor extends JFrame{
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JPanel colorPanel;
	private JSlider redSlider;
	private JSlider greenSlider;
	private JSlider blueSlider;
	private JRadioButton redButton;
	private JRadioButton greenButton;
	private JRadioButton blueButton;
	
	public RadioButtonColor(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		setSampleColor();
		createRadioButton();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class ColorListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
	}
	private void setSampleColor() {
		// TODO Auto-generated method stub
		redButton = new JRadioButton("Red");
		greenButton = new JRadioButton("Green");
		blueButton = new JRadioButton("Blue");
		if(redButton.isSelected()){
			colorPanel.setBackground(Color.RED);
		}
		else if(greenButton.isSelected()){
			colorPanel.setBackground(Color.GREEN);
		}
		else if(blueButton.isSelected()){
			colorPanel.setBackground(Color.BLUE);
		}
		colorPanel.repaint();
	}

	
	public JPanel createRadioButton(){
		AddColorListener listener = new AddColorListener();
		redButton = new JRadioButton("Red");
		redButton.addActionListener(listener);
		
		greenButton = new JRadioButton("Green");
		greenButton.addActionListener(listener);
		
		blueButton = new JRadioButton("Blue");
		blueButton.addActionListener(listener);
		blueButton.setSelected(true);;
		
		ButtonGroup group = new ButtonGroup();
		group.add(redButton);
		group.add(greenButton);
		group.add(blueButton);
		
		JPanel panel = new JPanel();
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		//panel.setBorder(new Border(new EtchedBorder(), "color"));
		add(panel, BorderLayout.SOUTH);
		
		return panel;
	}
}
