package ColorFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorButton extends JFrame {
	private static final int FRAME_HEIGHT = 200;
	private static final int FRAME_WIDTH = 400;
	private JPanel colorPanel;
	private JLabel txtColor;
	
	private JButton button1;
	private JButton button2;
	private JButton button3;
	
	public ColorButton(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		setSampleColor();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		
	}
	
	class ColorListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
	}
	
	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
		
	}

	public void setSampleColor() {
		// TODO Auto-generated method stub
		
		class AddColorListener implements ActionListener{
		       public void actionPerformed(ActionEvent event){
		    	   String arg = event.getActionCommand(); 
		   		if(event.getSource()instanceof JButton)
		   		{
		   			if("Red".equals(arg))
		   			{				
		   				colorPanel.setBackground(Color.RED);
		   			}
		   			
		   			else if("Green".equals(arg))
		   			{				
		   				colorPanel.setBackground(Color.GREEN);
		   			}
		   			
		   			else if("Blue".equals(arg))
		   			{				

		   				colorPanel.setBackground(Color.BLUE);
		   			}

		   		}	
	}
	
	private void createButton(){
		button1 = new JButton("Red");
		button2 = new JButton("Green");
		button3 = new JButton("Blue");
		
		AddColorListener listener = new AddColorListener();
		button1.addActionListener(listener);
		button2.addActionListener(listener);
		button3.addActionListener(listener);
		}
	}
	
	}
}