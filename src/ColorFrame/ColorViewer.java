package ColorFrame;

import javax.swing.JFrame;

public class ColorViewer {
	public static void main(String[] args){
		JFrame frame = new ColorButton();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("                                   Color Viewer ");
		frame.setVisible(true);
		
		JFrame frame1 = new ComboBoxColor();
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setTitle("                                  Color Viewer ");
		frame1.setVisible(true);
		
		JFrame frame2 = new RadioButtonColor();
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.setTitle("                                  Color Viewer ");
		frame2.setVisible(true);
		
		JFrame frame3  = new CheckBoxColor();
		frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame3.setTitle("                                  Color Viewer ");
		frame3.setVisible(true);
		
		JFrame frame4  = new MenuColor();
		frame4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame4.setTitle("                                  Color Viewer ");
		frame4.setVisible(true);
	}
}
