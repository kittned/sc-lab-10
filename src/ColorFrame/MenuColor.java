package ColorFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MenuColor extends JFrame{
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JPanel colorPanel;
	private JMenuItem Reds, Greens, Blues;
	private String facename;
	
	public MenuColor(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(createFileMenu());
		menuBar.add(createColorMenu());
		createFrameItem(facename);
		setSampleColor();
		
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	public JMenu createFileMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("File");
		menu.add(createFileNewItem());
		menu.add(createFileExitItem());
		return menu;
	}
	
	public JMenu createColorMenu() {
		// TODO Auto-generated method stub
		JMenu menu = new JMenu("Frame");
		menu.add(createFrameMenu());
		return menu;
	}
	
	public JMenuItem createFileNewItem(){
		JMenuItem item = new JMenuItem("New");
		 class MenuItemListener implements ActionListener{
			 public void actionPerformed(ActionEvent event){
				 setSampleColor();
			 }
		 }
		 	ActionListener listener = new MenuItemListener();
		 	item.addActionListener(listener);
		 	return item;
	}
	
	public JMenuItem createFileExitItem()
	{
	 JMenuItem item = new JMenuItem("Exit");
	 class MenuItemListener implements ActionListener{
		 public void actionPerformed(ActionEvent event){
			 System.exit(0);
		 }
	 }
	 	ActionListener listener = new MenuItemListener();
	 	item.addActionListener(listener);
	 	return item;
	 }
	
	public JMenu createFrameMenu()
	 {
		 
		 JMenu menu = new JMenu("Colors");
		 menu.add(Reds = new JMenuItem("Red"));
		 menu.add(Greens = new JMenuItem("Green"));
		 menu.add(Blues = new JMenuItem("Blue"));
	 return menu;
	 }
	
	public JMenuItem createFrameItem(final String name)
	 {
		JMenuItem item = new JMenuItem(name);
		 class MenuItemListener implements ActionListener{
		 public void actionPerformed(ActionEvent event)
		 {
			 if(event.getSource() == Reds){
				 colorPanel.setBackground(Color.red);
				 facename = name;
				 //setSampleColor();
			 }
			 else if(event.getSource() == Greens){
				 colorPanel.setBackground(Color.green);
				 facename = name;
			 }
			 else if(event.getSource() == Blues){
				 colorPanel.setBackground(Color.blue);
				 facename = name;
			 }
		 }
		 }
		 ActionListener listener = new MenuItemListener();
		 item.addActionListener(listener);
		 return item;
	 }

	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
		}
	}
	class ColorListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
	}
	
	private void setSampleColor() {
		// TODO Auto-generated method stub
		colorPanel.setBackground(Color.white);
		colorPanel.repaint();
	}

	
}
