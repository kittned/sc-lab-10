package ColorFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CheckBoxColor extends JFrame{
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JPanel colorPanel;
	private JCheckBox redButton;
	private JCheckBox greenButton;
	private JCheckBox blueButton;
	
	public CheckBoxColor(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		setSampleColor();
		createCheckBoxes();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);

	}
	

	private JPanel createCheckBoxes() {
		// TODO Auto-generated method stub
		AddColorListener listener = new AddColorListener();
		redButton = new JCheckBox("Red");
		redButton.addActionListener(listener);
		
		greenButton = new JCheckBox("Green");
		greenButton.addActionListener(listener);
		
		blueButton = new JCheckBox("Blue");
		blueButton.addActionListener(listener);
		
		JPanel panel = new JPanel();
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		panel.setBorder(new TitledBorder(new EtchedBorder(),"Style"));
		add(panel, BorderLayout.SOUTH);
		return panel;
	}

	private void setSampleColor() {
		// TODO Auto-generated method stub
		AddColorListener listener = new AddColorListener();
		redButton = new JCheckBox("Red");
		greenButton = new JCheckBox("Green");
		blueButton = new JCheckBox("Blue");
		if(redButton.isSelected()){
			colorPanel.setBackground(Color.RED);
		}
		else if(greenButton.isSelected()){
			colorPanel.setBackground(Color.GREEN);
		}
		else if(blueButton.isSelected()){
			colorPanel.setBackground(Color.BLUE);
		}
		colorPanel.repaint();
		
	}

	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			JCheckBox checkBox = ( JCheckBox ) event.getSource ( );	

             boolean isSelected = checkBox.isSelected ( ); 

             if ( isSelected ) 
             { 
                  JOptionPane.showMessageDialog (null, "You selected." ); 
             } 
             else 
             { 
                  JOptionPane.showMessageDialog (null, "You un-selected." ); 
             } 
		} 
		
	}
	class ColorListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
	}

	
}
