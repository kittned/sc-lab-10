package ColorFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ComboBoxColor extends JFrame{
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JPanel colorPanel;

	private JComboBox colorCombo;
	private JComboBox redButton;
	private JComboBox greenButton;
	private JComboBox blueButton;
	
	public ComboBoxColor(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		createControlPanel();
		setSampleColor();
		createComboBox();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class AddColorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
		}
	}
	class ColorListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent event) {
			// TODO Auto-generated method stub
			setSampleColor();
		}
	}
	
	private JPanel createComboBox() {
		// TODO Auto-generated method stub
		AddColorListener listener = new AddColorListener();
		colorCombo = new JComboBox();
		colorCombo.addItem("Red");
		colorCombo.addItem("Green");
		colorCombo.addItem("Blue");
		colorCombo.setEditable(true);
		colorCombo.addActionListener(listener);
		
		JPanel panel = new JPanel();
		panel.add(colorCombo);
		add(panel, BorderLayout.SOUTH);
		return panel;
	}

	private void setSampleColor() {
		// TODO Auto-generated method stub
		int num1 = 0;
		int num2 = 1;
		class AddInterestListener implements ActionListener{
		       public void actionPerformed(ActionEvent event){
		if(event.getSource()  == redButton){
			colorPanel.setBackground(Color.red);
		}
		
		else if(event.getSource()  == greenButton){
			colorPanel.setBackground(Color.green);
		}
		
		else if(event.getSource()  == blueButton){
			colorPanel.setBackground(Color.blue);
		}
		colorPanel.repaint();
		}
	}
	}

	private void createControlPanel() {
		// TODO Auto-generated method stub
		JPanel ComboBoxPanel = createComboBox();
		ChangeListener listener = new ColorListener();
		
	}
	
}
